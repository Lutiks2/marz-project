<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="section-404">
            <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 main-404">
                    <h1 class="title404">404</h1>
                </div>
                <div class="col-12 col-lg-6 main-404">
                    <p class="oops">oops!</p>
                    <p class="sorry">Sorry, the page not found!</p>
                    <div class="anchor-block">
                        <a class="anchor" href="/">
                            Back to home
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->
<?php wp_footer(); ?>

