<?php
?>

<section class="lite-bg job-section">
<!--    <div class="container">-->
        <div class="container descr-container">
            <h3 class="heading-title"><?php the_sub_field('title_section'); ?></h3>
            <?php

            $posts = get_sub_field('job');

            if( $posts ): ?>
                <ul class="job-list">
                    <?php foreach( $posts as $post):?>
                        <?php setup_postdata($post); ?>
                        <li class="job-list-item">
                            <h5 class="job-list-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h5>
                            <div class="exerpt"><?php the_excerpt(); ?></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php wp_reset_postdata();?>
            <?php endif; ?>
        </div>
<!--    </div>-->
</section>
