<div class="row">
    <?php
    if ($cur_term) {
        $paged_slug = 'page';
    } else {
        $paged_slug = 'paged';
    }
    $paged = get_query_var($paged_slug) ? absint(get_query_var($paged_slug)) : 1;
    $args = array(
        'post_type' => 'resources',
        'posts_per_page' => 2,
        'post_status' => 'publish',
        'paged' => $paged,
        'tax_query' => [
            [
                'taxonomy' => 'resourcestypes',
                'field'    => 'id',
                'terms'    => $tax_id,
            ]
        ]
    );
    if ($cur_term && $cur_term->term_id) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'resourcestypes',
                'field' => 'id',
                'terms' => $cur_term->term_id,
            ),
        );
    }


    $query = new WP_Query($args);
    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();


            $getslugid = wp_get_post_terms(get_the_ID(), 'resourcestypes');

            if ($getslugid[0]->slug == 'links') {
                require get_template_directory() . '/sections/resources/content-link.php';
            } elseif ($getslugid[0]->slug == 'videos') {
                require get_template_directory() . '/sections/resources/content-video.php';
            } else {
                require get_template_directory() . '/sections/resources/content-article.php';
            }


        }
        wp_reset_postdata();

    }
    //    wp_reset_query();
    ?>
</div>
<div class="nav-marz">

    <?php
    $big = 999999999; // уникальное число

    echo paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?' . $paged_slug . '=%#%',
        'current' => max(1, get_query_var($paged_slug)),
        'total' => $query->max_num_pages,
//            'mid_size' => 1,
        'end_size' => 4,
        'prev_text' => 'Prev',
        'next_text' => 'Next',
    ));
    ?>
</div>
<div class="wrap-cloud-technolog">
    <?php require get_template_directory() . '/sections/cloud-technologies-lite.php'; ?>
</div>
<?php //require get_template_directory() . '/sections/cloud-technologies-lite.php'; ?>
<style>
    iframe {
        max-width: 100%;
    }
</style>
