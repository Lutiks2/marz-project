<?php
/**
 * Template Name: Resources Type
 *
 */

get_header();

$cur_term = get_queried_object();

?>


    <div id="primary" class="content-area about">
        <div id="main" class="site-main">
            <?php
            $query = new WP_Query('page_id=644');
            if( $query->have_posts() ){
                while( $query->have_posts() ){
                    $query->the_post();
                    while (have_rows('resources_page')) : the_row(); ?>

                        <?php if (get_row_layout() == 'resources_page_welcome'): ?>

                            <?php require get_template_directory() . '/sections/resources/resources-welcome.php'; ?>

                        <?php endif; ?>
                    <?php endwhile;
                }
                wp_reset_postdata();
            }
            ?>

            <?php  require get_template_directory() . '/sections/resources/resources-filter.php'; ?>

            <article class="container">

                <?php  require get_template_directory() . '/sections/resources/resources-content-list.php'; ?>

            </article>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();