<?php
/**
 * theme enqueue scripts
 *
 * @package theme_name
 */

if ( ! function_exists( 'theme_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function theme_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );
		$css_version = $theme_version . '.' . filemtime(get_template_directory() . '/css/theme.min.css');


        //animate styles
        wp_enqueue_style( 'theme-styles-critical', get_stylesheet_directory_uri() . '/css_optimized/theme-critical.min.css', array(), $css_version );

        wp_register_script( 'jquery331', "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js", array(), 1, TRUE);
        wp_enqueue_script('jquery331');
        wp_register_script( 'slick', "https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js", array(), 1, TRUE);
        wp_enqueue_script('slick');


        $js_version = $theme_version . '.' . filemtime(get_template_directory() . '/js/main.min.js');
		wp_enqueue_script( 'v-scripts', get_template_directory_uri() . '/js/main_v.js', array(), $js_version, true );
		wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/js/vendor/wow.min.js', array(), $js_version, true );
		wp_enqueue_script( 'autosize', get_template_directory_uri() . '/js/vendor/autosize.min.js', array(), $js_version, true );
		wp_enqueue_script( 'vivus', get_template_directory_uri() . '/js/vendor/vivus.min.js', array(), $js_version, true );
        wp_enqueue_script( 'theme-scripts', get_template_directory_uri() . '/js/main.min.js', array(), $js_version, true );



	}
} // endif function_exists( 'theme_scripts' ).

add_action( 'wp_enqueue_scripts', 'theme_scripts' );


add_filter( 'excerpt_more', 'new_excerpt_more' );
function new_excerpt_more( $more ){
    global $post;
    return '<a class="btn-exerpt" href="'. get_permalink($post) . '">Reed more</a>';
}

add_filter( 'excerpt_length', function(){
    return 29;
} );


function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Vacancy';
    $submenu['edit.php'][5][0] = 'Vacancy';
    $submenu['edit.php'][10][0] = 'Add Vacancy';
    $submenu['edit.php'][16][0] = 'Vacancy Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Vacancy';
    $labels->singular_name = 'Vacancy';
    $labels->add_new = 'Add Vacancy';
    $labels->add_new_item = 'Add Vacancy';
    $labels->edit_item = 'Edit Vacancy';
    $labels->new_item = 'Vacancy';
    $labels->view_item = 'View Vacancy';
    $labels->search_items = 'Search Vacancy';
    $labels->not_found = 'No Vacancy found';
    $labels->not_found_in_trash = 'No Vacancy found in Trash';
    $labels->all_items = 'All Vacancy';
    $labels->menu_name = 'Vacancy';
    $labels->name_admin_bar = 'Vacancy';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );
