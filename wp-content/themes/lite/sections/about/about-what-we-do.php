<?php

?>

<section class="container-universal container">
    <div class="inside-container">
        <h3 class="title-section big-line"><?php the_sub_field('title_section'); ?></h3>
    </div>
    <img class="inside-img" src="<?php the_sub_field('image_section'); ?>">
    <div class="inside-container">
        <div class="descriptions"><?php the_sub_field('description_section'); ?></div>
    </div>
</section>


