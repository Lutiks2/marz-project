<?php
/**
 * * Template Name: Contact Us
 *
 */

get_header('lite'); ?>
    <div id="primary" class="content-area about">
        <main id="main" class="site-main">


            <div class="container">
                <?php while (have_rows('sections')) : the_row(); ?>

                    <?php if (get_row_layout() == 'hero_section'): ?>
                        <section class="hero_section-contact-us">

                            <div class="main-title contact-us-title">
                                <?php the_title(); ?>
                            </div>

                            <div class="row description-contact-us">

                                <div class="col-lg-7 col-12">
                                    <?php the_sub_field('description'); ?>
                                </div>

                                <div class="offset-lg-2 col-lg-3 col-12">

                                    <?php while (have_rows('info_section')) : the_row(); ?>

                                        <div class="mail-block">
                                            <?php if (get_sub_field('title')) : ?>
                                                <div class="mail-block-title">
                                                    <?php the_sub_field('title'); ?>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (get_sub_field('email')) : ?>
                                                <div class="mail-block-email">
                                                    <a href="mailto:<?php the_sub_field('email'); ?>">
                                                        <?php the_sub_field('email'); ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                    <?php endwhile; ?>
                                </div>

                            </div>

                            <div class="image-contact-block">
                                <div class="image-wrap">
                                    <?php
                                    $image = get_sub_field('background_image');
                                    if (!empty($image)): ?>

                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>

                                    <?php endif; ?>
                                </div>
                            </div>

                        </section>

                    <?php elseif (get_row_layout() == 'office_section'): ?>


                        <section class="office_section">

                            <div class="row">

                                <?php while (have_rows('office')) : the_row(); ?>

                                    <div class="col-lg-6 col-12 office_section-block small-line">

                                        <div class="address-block">
                                            <div class="address-block-title">
                                                <?php the_sub_field('title'); ?>
                                            </div>

                                            <div class="address-info-block">
                                                <div class="col-sm-6 col-12 address">
                                                    <?php the_sub_field('address'); ?>
                                                </div>
                                                <div class="col-sm-6 col-12 numbers_block">
                                                    <?php while (have_rows('numbers_block')) : the_row(); ?>
                                                        <div class="numbers">
                                                            <a href="tel:<?php the_sub_field('telephone_or_fax'); ?>"><?php the_sub_field('telephone_or_fax'); ?></a>
                                                        </div>
                                                    <?php endwhile; ?>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="location_section">
                                            <?php if (get_sub_field('map')) : ?>

                                                <div class="location_map">
                                                    <div class="map-top-block" onmousedown="mapScrollHandler(event)">
                                                        <span>For scrolling map hold the mouse or tap on map
                                                        </span>
                                                    </div>
                                                    <?php the_sub_field('map'); ?>

                                                </div>
                                            <?php endif; ?>
                                        </div>


                                    </div>


                                <?php endwhile; ?>

                            </div>

                        </section>


                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
            <div class="contact-us-border"></div>


        </main><!-- #main -->
    </div><!-- #primary -->


<?php
get_footer();
