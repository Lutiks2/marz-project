<?php
/**
 * Template Name: Homepage
 */

get_header('lite');
?>

    <div id="primary" class="content-area front-page">
        <main id="main" class="site-main">
            <?php while (have_rows('sections')) : the_row(); ?>


                <?php if (get_row_layout() == 'welcomes_section'): ?>

                    <?php require get_template_directory() . '/sections/homepage/welcome.php'; ?>

                <?php elseif (get_row_layout() == 'help_section'): ?>

                    <?php require get_template_directory() . '/sections/homepage/help.php'; ?>

                <?php elseif (get_row_layout() == 'technologies_section'): ?>

                    <?php require get_template_directory() . '/sections/homepage/technologies.php'; ?>

                <?php elseif (get_row_layout() == 'testimonials_section'): ?>

                    <?php require get_template_directory() . '/sections/homepage/testimonials.php'; ?>

                <?php elseif (get_row_layout() == 'patrons_section'): ?>

                    <?php require get_template_directory() . '/sections/homepage/patrons.php'; ?>

                <?php elseif (get_row_layout() == 'cloud_technologies_section'): ?>

                    <?php require get_template_directory() . '/sections/cloud-technologies.php'; ?>

                <?php endif; ?>
            <?php endwhile; ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
