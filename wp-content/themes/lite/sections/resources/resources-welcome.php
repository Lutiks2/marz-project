<?php
/**
 * section resources-welcome
 */
?>

<section>
    <div class="dark-blue-bg">
        <div class="container hero">
            <h1 class="page-name"><?php the_sub_field('resources_page_name'); ?></h1>
            <h2 class="main-title"><?php the_sub_field('resources_page_title'); ?></h2>
            <div class="bg">
                <img src="<?php the_sub_field('resources_page_image'); ?>">
                <!--                <div class="fon"></div>-->
            </div>
        </div>
    </div>
</section>

