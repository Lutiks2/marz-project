<?php
/**
 * section resources-filter
 */
?>

<section>
    <div class="" id="posts-content">
        <div class="container hero">

            <form action="<?php echo admin_url() ?>" method="POST" class="filter" id="filter">
                <?php
                $terms = get_terms(array(
                    'taxonomy' => 'types',
                    'hide_empty' => false,
                ));

                $types = '';
                $types = $_GET['types'];

                global $wp;
                 $current_slug =$_SERVER['REQUEST_URI'];
                $current_slug =strtok($current_slug, "&");
                ?>

                <ul class="filter-content">
                    <li class="filter-link <?php if($term->slug == $types): echo 'active'; endif;?>">
                        <a href="<?php echo $current_slug?>&types=">
                           All
                        </a>
                    </li>
                    <?php
                    foreach ($terms as $term) :
                        ?>
                        <li class="filter-link <?php if($term->slug == $types): echo 'active'; endif;?>">
                            <a href="<?php echo $current_slug?>&types=<?php echo $term->slug ?>">
                                <?php echo $term->name ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </form>

        </div>
    </div>
</section>