<?php

?>

<section class="help dark-blue-bg">
                <div class="container">
                    <div class="heading">
                        <h2 class="heading-title home-title"><?php the_sub_field('heading_for_section_help'); ?></h2>
<p class="heading-description"><?php the_sub_field('description_for_section_help'); ?></p>
</div>

<?php
$i = 0;
while (have_rows('helps_section')) : the_row();
    $i++;
    ?>
    <div class="<?php if ($i % 2 == 0) {
        echo 'item-wrap-reverse';
    } else {
        echo 'item-wrap';
    } ?>">
                <div class="row help-list">
                    <div class="col-12 col-lg-6 help-img wow" data-wow-offset="250" style="background-image: url(<?php the_sub_field('image_section_help'); ?>)">
                    </div>
                    <div class="help-list-content col-12 col-lg-6 wow" data-wow-offset="250" >
                        <h3 class="help-list-content-title small-line"><?php the_sub_field('subheading_section_help'); ?></h3>
                        <p class="help-list-content-description"><?php the_sub_field('description_help_list'); ?></p>
                    </div>
                </div>

    </div>

<?php endwhile; ?>

</div>

</section>