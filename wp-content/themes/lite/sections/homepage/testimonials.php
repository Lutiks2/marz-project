<?php

?>
<section class="lite-bg section testimonials">
    <div class="container">
        <div class="heading">
            <h3 class="heading-title home-title"><?php the_sub_field('section_title'); ?></h3>
        </div>
        <div class="one-time">
            <?php
            while (have_rows('testimonial_slide')) : the_row(); ?>

                <div>
                    <div class="slide">
                        <div class="slide-content"><?php the_sub_field('testimonial_content'); ?></div>
                        <p class="slide-representative"><?php the_sub_field('сompany_representative'); ?></p>
                        <p class="slide-company-name"><?php the_sub_field('company_name'); ?></p>
                    </div>
                </div>

            <?php endwhile;
            wp_reset_query();
            ?>
    </div>
    </div>
</section>

