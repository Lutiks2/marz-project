<?php
/**
 * Created by PhpStorm.
 * User: pixelshuttle1
 * Date: 28.12.2018
 * Time: 11:50
 */
?>


<form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
    <div>
        <input class="searching-text" type="text" value="<?php echo get_search_query() ?>" name="s" id="s"/>
        <input  class="searching-submit" type="submit" id="searchsubmit" value="Search"/>
    </div>
</form>