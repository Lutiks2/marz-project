<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 *
 *
 */

?>

<!--<h3>--><?php //the_title(); ?><!--</h3>-->
<!--<a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->

<div class="our-works-img">
    <?php the_post_thumbnail(); ?>
</div>
<div class="our-works-content">
    <a href="<?php the_permalink(); ?>">
        <p><?php the_content(); ?></p>
        <h3><?php the_title(); ?></h3>
    </a>
</div>


