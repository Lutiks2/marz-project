<?php
/**
 * * Template Name: Solutions

 */

get_header(); ?>
    <div id="primary" class="content-area solutions">
        <main id="main" class="site-main">
            <?php while (have_rows('solutions_page')) : the_row(); ?>

                <?php if (get_row_layout() == 'solutions_welcome'): ?>

                    <?php require get_template_directory() . '/sections/solutions/solutions-welcome.php'; ?>

                <?php elseif (get_row_layout() == 'provide_servises'): ?>

                    <?php require get_template_directory() . '/sections/solutions/provide.php'; ?>

                <?php elseif (get_row_layout() == 'solutions_servises'): ?>

                    <?php require get_template_directory() . '/sections/solutions/solutions-servises.php'; ?>

                <?php elseif (get_row_layout() == 'solutions_cloud_technologies'): ?>

                    <?php require get_template_directory() . '/sections/cloud-technologies.php'; ?>

                <?php endif; ?>
            <?php endwhile; ?>

        </main><!-- #main -->
    </div><!-- #primary -->


<?php
get_footer();
