<?php
/**
 *The template for displaying book posts.
 *
 * @package theme_name
 */

get_header('lite');
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="container">

            <div class="anchor-block">
                <a class="anchor" href="/resources/">
                    Back to list
                </a>
            </div>

            <?php while (have_posts()) : the_post(); ?>

                <div class="page-settings">


<!---->
                    <h1 class="main-title">
                        <?php the_title(); ?>
                    </h1>

                    <div class="content-page"><?php the_content(); ?></div>

                </div>

            <?php endwhile; ?>

            <p>Single post</p>
        </div>
    </main>
</div>


<?php get_footer(); ?>
