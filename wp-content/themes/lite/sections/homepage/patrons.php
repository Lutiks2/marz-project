<?php
?>

<section class="patrons-section dark-blue-bg">
    <div class="container">
        <div class="patrons-section-heading">
            <h2 class="patrons-section-heading-title"><?php the_sub_field('head_for_section_our_patrons'); ?></h2>
        </div>
        <div>
            <?php
            $images = get_sub_field('patrons_gallery');

            if ($images): ?>

            <div class="row">

                <ul class="patrons-list ">
                    <?php foreach ($images as $image): ?>
                        <li class="col-xl-2dot4 col-md-3 col-sm-4 col-6 patrons">
                            <img class="patrons-img" src="<?php echo $image['sizes']['thumbnail']; ?>"
                                 alt="<?php echo $image['alt']; ?>"/>
                        </li>
                    <?php endforeach; ?>
                </ul>

            </div>

            <?php endif; ?>
        </div>
    </div>
</section>
