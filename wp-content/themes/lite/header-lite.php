<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *

 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site sticky">

    <header id="masthead" class="site-header lite-header">
        <div class="sticky-main">
            <div class=" site-branding header-block">
                <?php if (is_front_page()) : ?>
                    <div class="site-title screen-reader-text">
                        <div class="menu-view"><?php the_custom_logo(); ?></div>
                    </div>
                <?php else : ?>
                    <div class="menu-view"><?php the_custom_logo(); ?></div>
                <?php endif; ?>

                <!-- .site-branding -->

                <nav id="site-navigation" class="main-navigation">
                    <div class="but-menu">
                        <a class="menu-btn" href="#">
                            <span></span>
                        </a>
                    </div>
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'header',
                        'menu_id' => 'header',
                        'menu_class' => 'header-navigation',
                    ));
                    ?>
                </nav><!-- #site-navigation -->
            </div>
        </div>

    </header><!-- #masthead -->

    <div id="content" class="site-content">
