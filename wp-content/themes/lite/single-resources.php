<?php
/**
 * The template for displaying all single posts.
 *
 * @package theme_name
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="container">
            <div class="anchor-block">
                <a class="anchor" href="/resources/">
                    Back to list
                </a>
            </div>
            <!--            --><?php //the_taxonomies($args); ?>
            <?php while (have_posts()) : the_post(); ?>
                <?php $getslugid = wp_get_post_terms(get_the_ID(), 'resourcestypes');
                if ($getslugid[0]->slug == 'videos') { ?>
                    <div class="page-settings">
                        <h1 class="main-title">
                            <?php the_title(); ?>
                        </h1>
                        <div class="wrap-video">
                            <div class="image-embed post-image"
                                 style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>); width: 100%; background-size: cover;">
                            </div>

                            <?php

                            // get iframe HTML
                            $iframe = get_field('video');


                            // use preg_match to find iframe src
                            preg_match('/src="(.+?)"/', $iframe, $matches);
                            $src = $matches[1];


                            // add extra params to iframe src
                            $params = array(
                                'controls' => 0,
                                'hd' => 1,
                                'autohide' => 1,
                                'width' => 800,
                                'autoplay'   => 1
                            );

                            $new_src = add_query_arg($params, $src);

                            $iframe = str_replace($src, $new_src, $iframe);


                            // add extra attributes to iframe html
                            $attributes = 'frameborder="0"';

                            $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);


                            // echo $iframe
//                            echo $iframe;

                            ?>
                            <a id="play-btn" data-video-embed="<?php echo htmlspecialchars($iframe) ?>">
                                <img class="play-btn"
                                     src="<?php echo get_template_directory_uri() . '/images/icons/button-play.svg' ?>"
                                     alt="Play">
                            </a>

                            <div class="home-embed-container">
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="page-settings">

                        <h1 class="main-title">
                            <?php the_title(); ?>
                        </h1>
                        <div>
                            <?php if (get_the_post_thumbnail()): ?>
                                <div class="post-image"
                                     style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>
                                             ); width: 100%; background-size: cover;">
                                </div>
                                <!--                            <figure class="post-img">-->
                                <!--                                --><?php //the_post_thumbnail(); ?>
                                <!--                            </figure>-->
                            <?php else : ?>
                                <div class="post-image"
                                     style="background-color:#0A246A; background-image: url(<?php echo get_template_directory_uri() . '/images/default-post-img.jpg' ?>); width: 100%; height: 100px;">
                                </div>

                            <?php endif; ?>
                        </div>
                        <div class="content-page"><?php the_content(); ?></div>

                    </div>
                <?php } ?>

            <?php endwhile; ?>

            <div>
                <?php

                $posts = get_field('related_posts');

                if ($posts): ?>
                    <div class="row">
                        <h3 class="col-12 related-title">Related posts</h3>
                    </div>
                    <?php require get_template_directory() . '/sections/resources/related-post.php'; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>

        </div>
        <?php require get_template_directory() . '/sections/cloud-technologies-lite.php'; ?>
    </main>
</div>


<?php get_footer(); ?>
