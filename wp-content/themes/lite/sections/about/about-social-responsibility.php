<?php
?>

<section class="responsibility">
    <div class="container">
        <div class="responsibility-inner lite-blue-bg">
            <img class="responsibility-image" src="<?php the_sub_field('image_section'); ?>">
            <div class="responsibility-text">
                <h3 class="title-section small-line"><?php the_sub_field('title_section'); ?></h3>
                <div class="descriptions"><?php the_sub_field('description_section'); ?></div>
            </div>
        </div>
    </div>
</section>
