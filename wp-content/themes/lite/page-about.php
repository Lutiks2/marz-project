<?php
/**
 * * Template Name: About
 *
 */

get_header(); ?>
    <div id="primary" class="content-area about">
        <main id="main" class="site-main">
            <?php while (have_rows('about_page')) : the_row(); ?>

                <?php if (get_row_layout() == 'about_welcome'): ?>

                    <?php require get_template_directory() . '/sections/about/about-welcome.php'; ?>

                <?php elseif (get_row_layout() == 'about_history'): ?>

                    <?php require get_template_directory() . '/sections/about/about-history.php'; ?>

                <?php elseif (get_row_layout() == 'about_what_we_do'): ?>

                    <?php require get_template_directory() . '/sections/about/about-what-we-do.php'; ?>

                <?php elseif (get_row_layout() == 'about_mission'): ?>

                    <?php require get_template_directory() . '/sections/about/about-mission.php'; ?>

                <?php elseif (get_row_layout() == 'about_quality'): ?>

                    <?php require get_template_directory() . '/sections/about/about-quality.php'; ?>

                <?php elseif (get_row_layout() == 'about_social_responsibility'): ?>

                    <?php require get_template_directory() . '/sections/about/about-social-responsibility.php'; ?>

                <?php elseif (get_row_layout() == 'about_members'): ?>

                    <?php require get_template_directory() . '/sections/about/about-members.php'; ?>

                <?php elseif (get_row_layout() == 'about_cloud_technologies'): ?>

                    <?php require get_template_directory() . '/sections/cloud-technologies.php'; ?>

                <?php endif; ?>
            <?php endwhile; ?>

        </main><!-- #main -->
    </div><!-- #primary -->


<?php
get_footer();
