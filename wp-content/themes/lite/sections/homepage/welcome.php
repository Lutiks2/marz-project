<?php ?>

<section class="welcome-frontpage" style="background-image: url(<?php the_sub_field('welcome_image'); ?>)">
    <div class="container welcome_main"><!--   text on welcome section-->
                <h1 class="welcome-heading"><?php the_sub_field('welcome_title'); ?></h1>
                <p class="welcome-description"><?php the_sub_field('welcome_description'); ?></p>
                <div>
                    <div class="welcome-button">
<!--                    <a class="button button-header" href="mailto:--><?php //the_sub_field('welcome_button_email'); ?><!--">-->
                        <a class="button button-header" href="<?php the_sub_field('welcome_button_link'); ?>">
                            <?php the_sub_field('welcome_button_text'); ?>
                    </a>
                </div>
            </div>
</section>

