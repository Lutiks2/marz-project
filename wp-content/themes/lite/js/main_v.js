$(document).ready(function ($) {

//-----------Modal window----------//

    $('.button-cloud-technologies').on('click', function () {
        $('body').addClass('body-block')
        $('.sticky-main').addClass('dark-header')
    });

    $('#modal-one-close').on('click', function () {
        $('body').removeClass('body-block')
      $('body').removeClass('modal-dialog-get')
            clickReload();
    });


  function clickReload() {

    $('.modal-content-get').show();
    $('.thank-you-block').removeClass('d-flex');
    removeFile();
  }

//---------Style for contact from---------//


    function vivus() {
        new Vivus('submit-img', {
            duration: 50,
            type: 'sync',
            animTimingFunction: Vivus.EASE,
            start: 'inViewport'
        });
    }


    //-----Autoresize textarea
    autosize($('textarea'));


    //-----Change name of label to name of file
  if(document.getElementById('chooseFile')) {
    document.getElementById('chooseFile').onchange = function () {
      showname();
    };
  }
    function showname() {
        var name = document.getElementById('chooseFile');
        $('.label-file').text(name.files.item(0).name)
    }


    document.addEventListener('wpcf7mailsent', function (event) {
        if ('233' == event.detail.contactFormId) {
            $('.modal-content-get').hide();
            $('.thank-you-block').addClass('d-flex');
            vivus();
            // location = 'http://lite.local/careers/';
        }
        if ('564' == event.detail.contactFormId) {
            $('.modal-content-get').hide();
            $('.thank-you-block').addClass('d-flex');
            vivus();
        }

    }, true);

  function removeFile() {
    document.getElementById('chooseFile').value = "";
    document.querySelector('.label-file').innerText = "*Attach a file";
  }

  function createRemoveFileButton() {
      let fileAttach =  document.querySelector('.wpcf7-form-control-wrap.file-attach');

      if (fileAttach !== null) {
          let removeFileButton = document.createElement('div');
          removeFileButton.classList.add('remove-file-button');
          removeFileButton.addEventListener('click', removeFile);
          fileAttach.appendChild(removeFileButton) ;
      }
  }

  createRemoveFileButton();


//---------End submit for contact from----//

});