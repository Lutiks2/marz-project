<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer footer-marz">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-6">
                <div class="form-section">
                    <h3 class="heading-footer"><?php the_field('heading_footer', 'option'); ?></h3>
                    <p class="description-footer"><?php the_field('description_footer', 'option'); ?></p>
                    <div class="contact-form">
                        <?php echo do_shortcode('[contact-form-7 id="234" title="Subscribe"]'); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-6">
                <div class="row">
                    <address class="address-block col-12 col-md-6">
                        <a class="address-block-item-icon address-block-position telephone"
                           href="tel:<?php the_field('real_telephone', 'option'); ?>"><?php the_field('telephone', 'option'); ?></a>
                        <a class="address-block-item-icon address-block-position letter"
                           href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                        <div class="address-block-item-icon address-block-position marker"><?php the_field('address', 'option'); ?></div>
                        <p class="address-block-item-icon address-block-position work-time"><?php the_field('work_time', 'option'); ?></p>
                    </address>
                    <div class="social-section col-12 col-md-6">
                        <p class="description-social"><?php the_field('description_social', 'option'); ?></p>
                        <h5 class="heading-social-block"><?php the_field('heading_social_block', 'option'); ?></h5>

                        <div class="social-link-block">
                            <a class="social-link tvitter" target="_blank" href="<?php the_field('twitter_link', 'option'); ?>"></a>
                            <a class="social-link linkedin" target="_blank" href="<?php the_field('linkedin_link', 'option'); ?>"></a>
                            <a class="social-link facebook" target="_blank" href="<?php the_field('facebook_link', 'option'); ?>"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copiwrite">
            Copyright © 2003 - <?php echo date('Y'); ?> Marz Systems
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
