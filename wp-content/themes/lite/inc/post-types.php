<?php

function create_post_types()
{
    $resources_labels = array(
        'name' => __('Resources'),
        'singular_name' => __('Resources'),
        'add_new' => __('Add Resource'),
        'add_new_item' => __('Add New Resource'),
        'edit_item' => __('Edit Resource'),
        'new_item' => __('New Resource'),
        'view_item' => __('View Resource'),
        'search_items' => __('Search Resource'),
        'not_found' => __('Resources not found'),
        'not_found_in_trash' => __('Resources not found in Trash'),
        'parent_item_colon' => __('Parent Resource'),
        'menu_name' => __('Resources'),
    );
    $resources_args = array(
        'labels' => $resources_labels,
        'hierarchical' => false,
        'supports' => array("title", "thumbnail", "editor"),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-carrot',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
//        'rewrite' => array(
//            'slug' => 'resource',
//            'with_front' => true
//        ),
        'menu_position' => 15,
        'capability_type' => 'post'
    );
    register_post_type('resources', $resources_args);

}

add_action('init', 'create_post_types');

function create_taxonomy()
{

    $resourcetypes_labels = array(
        'name' => 'Resource Types',
        'singular_name' => 'Resource Type',
        'search_items' => 'Search Resource Types',
        'all_items' => 'All Resource Types',
        'parent_item' => 'Parent Resource Type',
        'parent_item_colon' => 'Parent Resource Type:',
        'edit_item' => 'Edit Resource Type',
        'update_item' => 'Update Resource Type',
        'add_new_item' => 'Add New Resource Type',
        'new_item_name' => 'New Resource Type Name',
        'menu_name' => 'Resource Types',
    );
    $resourcetypes_args = array(
        'label' => '',
        'labels' => $resourcetypes_labels,
        'description' => '',
        'public' => true,
        'publicly_queryable' => null,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_in_rest' => true,
        'hierarchical' => true,
        'update_count_callback' => '',
//        'rewrite' => true,
        'rewrite' => array(
            'slug'=>'resources',
            'with_front' => true
        ),
        'capabilities' => array(),
        'meta_box_cb' => null,
        'show_admin_column' => true,
        '_builtin' => false,
        'show_in_quick_edit' => null,
        'exclude_from_search' => 0,
    );
    register_taxonomy('resourcestypes', array('resources'), $resourcetypes_args);
}

add_action('init', 'create_taxonomy');


