<?php
/**
 * * Template Name: Resources
 *
 */

get_header(); ?>
    <div id="primary" class="content-area about">
        <main id="main" class="site-main">
            <?php while (have_rows('resources_page')) : the_row(); ?>

                <?php if (get_row_layout() == 'resources_page_welcome'): ?>

                    <?php require get_template_directory() . '/sections/resources/resources-welcome.php'; ?>

                <?php endif; ?>
            <?php endwhile; ?>
            <?php require get_template_directory() . '/sections/resources/resources-filter.php'; ?>


            <article class="container">

                <?php  require get_template_directory() . '/sections/resources/resources-content-list.php'; ?>

            </article>
        </main><!-- #main -->
    </div><!-- #primary -->


<?php
get_footer();
