<?php

?>

<section>
    <div class="dark-blue-bg">
        <div class="container hero">
            <h1 class="page-name"><?php the_sub_field('page_name'); ?></h1>
            <h2 class="main-title"><?php the_sub_field('solutions_title'); ?></h2>
            <div class="bg">
                <img src="<?php the_sub_field('solutions_header_image'); ?>">
<!--                <div class="fon"></div>-->
            </div>
        </div>
    </div>
    <div class="lite-bg">
        <div class="container descr-container">
            <div class="descriptions"><?php the_sub_field('solutions_descriptions'); ?></div>
        </div>
    </div>
</section>
