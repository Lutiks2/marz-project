<?php
?>
<section>
    <div class="" id="posts-content">
        <div class="container hero">
            <ul class="filter-content">
                <li class="filter-link  <?php if (!$cur_term->term_id){ echo 'active';};?>">
                    <a class="" href="/resources/"><span class="mdc-tab-label">All</span></a>
                </li>

                <?php
                $args = array(
                    'taxonomy' => 'resourcestypes',
                    'hide_empty' => true,
                    'parent' => 0,
                    'meta_query' => array(
                        array(
                            'key' => 'enabled_resources_type',
                            'compare' => '=',
                            'value' => '1'
                        )
                    )
                );
                $tax_id=[];
                $terms = get_terms($args);
                if ($terms && !is_wp_error($terms)) {

                    foreach ($terms as $term) {
                        array_push($tax_id,$term->term_id);
                        if ($cur_term->term_id && $term->term_id == $cur_term->term_id) {
                            $activclass = 'active';
                        } else {
                            $activclass = '';
                        }; ?>
                        <li class="filter-link <?php echo $activclass; ?>">
                            <a href="<?php echo get_term_link($term->term_id); ?>">
                                <span class="">
                                <?php echo $term->name; ?>
                            </span>
                            </a>
                        </li>

                        <?php
                    } ?>

                    <?php
                }
                ?>
            </ul>
            <?php
            ?>
        </div>
    </div>
</section>