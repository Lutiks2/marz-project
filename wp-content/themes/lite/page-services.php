<?php
/**
 * * Template Name: Services
 *
 */

get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <?php while (have_rows('services_page')) : the_row(); ?>

                <?php if (get_row_layout() == 'services_welcome'): ?>

                    <?php require get_template_directory() . '/sections/services/services-welcome.php'; ?>

                <?php elseif (get_row_layout() == 'it-services'): ?>

                    <?php require get_template_directory() . '/sections/services/services-provide.php'; ?>

                <?php elseif (get_row_layout() == 'services_tabs'): ?>

                    <?php require get_template_directory() . '/sections/services/services-tab.php'; ?>


                <?php elseif (get_row_layout() == 'services_cloud_technologies'): ?>

                    <?php require get_template_directory() . '/sections/cloud-technologies.php'; ?>

                <?php endif; ?>
            <?php endwhile; ?>

        </main><!-- #main -->
    </div><!-- #primary -->


<?php
get_footer();