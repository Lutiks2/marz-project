<?php
?>
<section class="lite-bg">
    <div class="container-universal container">
        <div class="descriptions">
            <h3 class="title-section big-line"><?php the_sub_field('title_section'); ?></h3>
        </div>
        <div class="descriptions"><?php the_sub_field('description_section'); ?></div>

    </div>
</section>
