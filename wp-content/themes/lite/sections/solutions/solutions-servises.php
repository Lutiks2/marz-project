<?php

?>


<!--mobile version-->
<section class="dark-blue-bg section solutions-servises solutions-servises-mobile">
    <div class="container ">
        <div class="">
            <?php
            while (have_rows('solutions_servises_item')) : the_row(); ?>
                <div class="solutions-description" id="scroll">
                    <h3 id="<?php echo sanitize_title(get_sub_field('solutions_servises_item_title')); ?>-mobile" class="solutions-description-title mobile-tab"><?php the_sub_field('solutions_servises_item_title'); ?></h3>
                    <div class="mobile-tab-item"><?php the_sub_field('solutions_servises_item_description'); ?></div>
                </div>
            <?php endwhile;
            wp_reset_query();
            ?>
        </div>
    </div>
</section>
<!--end mobile version-->
<!--desktop version-->
<section class="dark-blue-bg section solutions-servises servises-desktop">
    <div class="container">
        <div class="row">
            <ul class="solutions-servises-list col-md-5">
                <?php
                while (have_rows('solutions_servises_item')) : the_row(); ?>
                    <li class="tab solutions-servises-item">
                        <h3 id="<?php echo sanitize_title(get_sub_field('solutions_servises_item_title')); ?>" class="solutions-servises-item-title"><?php the_sub_field('solutions_servises_item_title'); ?></h3>
                    </li>
                <?php endwhile;
                wp_reset_query();
                ?>
            </ul>
            <div class="col-md-7 solutions-description-wrap" id="scrollTab">
                <?php
                while (have_rows('solutions_servises_item')) : the_row(); ?>
                    <div class="tab_item solutions-description" >
                        <h3 class="solutions-description-title"><?php the_sub_field('solutions_servises_item_title'); ?></h3>
                        <?php the_sub_field('solutions_servises_item_description'); ?>
                    </div>
                <?php endwhile;
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
</section>
<!--end desktop version-->
