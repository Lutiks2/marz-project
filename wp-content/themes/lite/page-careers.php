<?php
/**
 * * Template Name: Carrers
 *
 */

get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <?php while (have_rows('careers_page')) : the_row(); ?>

                <?php if (get_row_layout() == 'careers_welcome'): ?>

                    <?php require get_template_directory() . '/sections/careers/careers-welcome.php'; ?>

                <?php elseif (get_row_layout() == 'careers_job'): ?>

                    <?php require get_template_directory() . '/sections/careers/careers-list.php'; ?>

                <?php endif; ?>
            <?php endwhile; ?>
        </main><!-- #main -->
    </div><!-- #primary -->


<?php
get_footer();
