<?php
/**
 * The template for displaying all single posts.
 *
 * @package theme_name
 */

get_header('lite');
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <div class="container">
            <div class="anchor-block">
                <a class="anchor" href="/careers/">
                    Back to list
                </a>
            </div>

            <?php while (have_posts()) : the_post(); ?>

                <div class="page-settings">

                    <h1 class="main-title">
                        <?php the_title(); ?>
                    </h1>

                    <div class="content-page"><?php the_content(); ?></div>

                </div>

            <?php endwhile; ?>

            <div class="apply dark-blue-bg">
                <a class="apply-heading" href="#career-form">Apply Now</a>
            </div>

        </div>
    </main>
</div>

<?php while (have_posts()) : the_post(); ?>

<!-- Modal -->
<div class="modal-get" id="career-form" aria-hidden="true">
    <a href="#/" class="modal-overlay-get"></a>

    <div class="modal-dialog-get">
        <div class="modal-header-get">
            <a href="#/" class="btn-close" aria-hidden="true" id="modal-one-close">
                <img src="<?php echo get_template_directory_uri() ?>/images/icons/ic-close.svg" alt="">
            </a>
        </div>


        <div class="modal-content-get">
            <div class="modal-content-header">
                <h1 class="modal-content-header-title">
                    <?php the_title(); ?>
                </h1>

                <h5 class="modal-content-header-subtitle">
                    Join the Marz team by fill a simple form
                </h5>
            </div>

            <div class="form-get-in-block">
                <?php echo do_shortcode('[contact-form-7 id="564" title="Form for Vacancy"]'); ?>
            </div>
        </div>


        <!-- Thank you block -->

        <div class="thank-you-block">

            <div class="container">

                <div class="thank-you-text">

                    <?php echo getImage(get_template_directory_uri() . '/images/submit_1.svg'); ?>

                    <h1 class="thank-you-text-title">
                        <?php the_title(); ?>
                    </h1>

                    <div class="thank-description">
                        <?php the_field('description_thank_you', 'option'); ?>
                    </div>

                </div>

            </div>


        </div>

        <!-- /Thank you block -->

    </div>


</div>
<!-- /Modal -->

<?php endwhile; ?>


<?php get_footer(); ?>
