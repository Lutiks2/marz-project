<?php

?>
<?php get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="container">
                <?php if (have_posts()) {
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = [
                        'order' => 'ASC',
//                        'posts_per_page' => 2,
                        'paged' => $paged,
                    ];
                    query_posts($args);
                    while (have_posts()) : the_post(); ?>
                        <p>Category :
                            <?php $category = get_the_category();
                            echo $category[0]->cat_name; ?>
                        </p>
                        <p>
                            <?php the_modified_date('j F, Y'); ?>
                        </p>

                        <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                            <?php the_post_thumbnail(); ?>
                            <?php the_excerpt(); ?>
                        </div>

                    <?php endwhile; ?>
                    <div class="navigation">
                        <!--                        <div class="next-posts">-->
                        <?php //next_posts_link(); ?><!--</div>-->
                        <!--                        <div class="prev-posts">-->
                        <?php //previous_posts_link(); ?><!--</div>-->
                        <?php the_posts_pagination(); ?>
                    </div>
                    <?php wp_reset_query(); ?>
                    <?php
                } // конец if
                else
                    echo "<h2>Записей нет.</h2>";
                ?>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php

get_footer();