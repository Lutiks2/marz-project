<?php

?>

<section class="technologies" style="background-color:<?php echo get_theme_mod('tech_backgr_color'); ?>">
    <div class="container">
        <div class="heading">
            <h3 class="heading-title home-title"><?php the_sub_field('heading_for_section_technologies'); ?></h3>
            <p class="heading-description"><?php the_sub_field('description_for_section_technologies'); ?></p>
        </div>
        <ul class="row techno-list">
            <?php

            while (have_rows('technolist')) : the_row(); ?>
<!--                <li class="techno-list-li col-12 col-sm-6 col-md-4 wow fadeInUp" data-wow-offset="200" data-wow-delay="0.8s" data-wow-duration="2s">-->
                <li class="techno-list-li col-12 col-md-6 col-lx-4 wow fadeInUp" data-wow-offset="300">
<!--                <li class="techno-list-li col-12 col-sm-6 col-md-4 ">-->
                    <a class="techno-link" href="<?php the_sub_field('link_for_post_techno'); ?>">
                        <img class="techno-img" src="<?php the_sub_field('image_for_post_techno'); ?>" alt="help1"/>
                        <h4 class="techno-name"><?php the_sub_field('heading_for_post_techno'); ?></h4>
                    </a>

                </li>
            <?php endwhile;
            wp_reset_query();
            ?>
        </ul>
    </div>

</section>
