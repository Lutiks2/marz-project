<?php
?>

<section class="section dark-blue-bg members">
    <div class="container">
        <h3 class="heading-title"><?php the_sub_field('title_section'); ?></h3>
        <?php
        if (have_rows('members_list')):?>
            <ul class="members-list">

                <?php while (have_rows('members_list')) : the_row(); ?>

                    <li class="members-list-item">
                        <img class="members-photo" src="<?php the_sub_field('members_foto'); ?>">
                        <h4 class="members-name"><?php the_sub_field('members_name'); ?></h4>
                        <p class="members-position"><?php the_sub_field('members_position'); ?></p>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</section>
