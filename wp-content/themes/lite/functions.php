<?php
/**
 * theme functions and definitions
 *
 * @package theme_name
 */

/**
 * Theme setup and custom theme supports.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 *  Custom post type registrations.
 */
require get_template_directory() . '/inc/post-types.php';


/**
 * Remove admin nav bar
 */

show_admin_bar(false);


// Edit SVG background icon
function getImage($patch, $class = false)
{
    $arrContextOptions=array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    );
    if (strpos($patch, '.svg') > 0) {
        $url = $patch;
        $uploads = wp_upload_dir();
        $image_path = str_replace($uploads['baseurl'], $uploads['basedir'], $url);
        $content = file_get_contents($image_path, false, stream_context_create($arrContextOptions));
    } else {
        $content = ' <img class="' . $class . '" src="<' . $patch . '">';
    }
    return $content;
}

function taxonomy_rewrite_fix($wp_rewrite) {
    $r = array();
    foreach($wp_rewrite->rules as $k=>$v){
        $r[$k] = str_replace('resourcestypes=$matches[1]&paged=','resourcestypes=$matches[1]&page=',$v);
    }
    $wp_rewrite->rules = $r;
}
add_filter('generate_rewrite_rules', 'taxonomy_rewrite_fix');


function move_styles_to_footer() {

    $the_theme = wp_get_theme();
    $theme_version = $the_theme->get( 'Version' );
    $css_version = $theme_version . '.' . filemtime(get_template_directory() . '/css/theme.min.css');

//    wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/css_optimized/slick-local.min.css?ver=1.6.0', array(), null );
    wp_enqueue_style( 'slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css', array(), null );
//    wp_enqueue_style( 'slick-theme', get_stylesheet_directory_uri() . '/css_optimized/slick-theme-local.min.css?ver=1.6.0', array(), null );
    wp_enqueue_style( 'slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css', array(), null );

    wp_enqueue_style( 'theme-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), null );
    wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/wow/css/libs/animate.css');

    wp_enqueue_style( 'wp-block-library', '/wp-includes/css/dist/block-library/style.min.css', array(), $css_version );
    wp_enqueue_style( 'contact-form-7', '/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4', array(), $css_version );

    if(is_page()){ //attache styles, scripts to exact page
        attach_mapbox_to_page();
    }
};

function attach_mapbox_to_page() {
    $the_theme = wp_get_theme();
    $theme_version = $the_theme->get( 'Version' );
    $css_version = $theme_version . '.' . filemtime(get_template_directory() . '/css/theme.min.css');
    $js_version = $theme_version . '.' . filemtime(get_template_directory() . '/js/main.min.js');
    global $wp_query;

    //Check which template is assigned to current page we are looking at
    $template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );
    if($template_name == 'page-contact_us.php'){

//            wp_enqueue_script('mapbox_gl_js', get_template_directory_uri() . '/js_optimized/mapbox_gl_js_local.min.js', array(), $js_version, true);
//            wp_enqueue_style( 'mapbox_gl_js_css', get_stylesheet_directory_uri() . '/css_optimized/mapbox-gl-local.css', array(), $css_version );
//            wp_enqueue_style( 'mapbox_gl_js_geocoder_css', get_stylesheet_directory_uri() . '/css_optimized/mapbox-gl-geocoder-local.css', array(), $css_version );
//            wp_enqueue_style( 'mapbox_gl_js_directions_css', get_stylesheet_directory_uri() . '/css_optimized/mapbox-gl-directions-local.css', array(), $css_version );

        wp_enqueue_script('mapbox_gl_js', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.js?ver=5.2.3', array(), $js_version, true);
        wp_enqueue_style( 'wp-mapbox-gl-js', '/wp-content/plugins/wp-mapbox-gl-js/public/css/wp-mapbox-gl-js-public.css?ver=2.0.5', array(), $css_version );
        wp_enqueue_style( 'mapbox_gl_js_css', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.43.0/mapbox-gl.css?ver=5.2.3', array(), $css_version );
        wp_enqueue_style( 'mapbox_gl_js_geocoder_css', 'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.2.0/mapbox-gl-geocoder.css?ver=5.2.3', array(), $css_version );
        wp_enqueue_style( 'mapbox_gl_js_directions_css', 'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v3.1.1/mapbox-gl-directions.css?ver=5.2.3', array(), $css_version );
    }
}

function deregister_styles_and_scripts() {
    wp_deregister_style('wp-block-library');
    wp_deregister_style('contact-form-7');
    wp_deregister_style('mapbox_gl_js_css');
    wp_deregister_style('mapbox_gl_js_geocoder_css');
    wp_deregister_style('mapbox_gl_js_directions_css');
    wp_deregister_style('wp-mapbox-gl-js');
    wp_deregister_script( 'mapbox_gl_js' );
}

add_action('wp_print_styles', 'deregister_styles_and_scripts', 100);
add_action( 'get_footer', 'move_styles_to_footer' );


function eliminate_render_blocking_css($html, $handle, $href, $media) {
    if (is_admin())  return $html;

    if (strpos($href, 'theme-critical.min.css') === false)
    $html = <<<EOT
<link rel='preload' as='style' onload="this.onload=null;this.rel='stylesheet'"
id='$handle' href='$href' type='text/css' media='all' />
EOT;
    return $html;
}

add_filter( 'style_loader_tag', 'eliminate_render_blocking_css', 10, 4 );