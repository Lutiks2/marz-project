<?php
?>


<section class="provide container">
    <h3 class="heading-section"><?php the_sub_field('provide_servises_title'); ?></h3>
    <ul class="row techno-list">
        <?php

        while (have_rows('provide_servises_item')) : the_row(); ?>
            <li class="techno-list-li col-12 col-md-6 col-lx-4 wow fadeInUp" data-wow-offset="280">

                <a class="techno-link" href="<?php the_sub_field('link_for_services_item'); ?>">
                    <img class="techno-img" src="<?php the_sub_field('image_servises_item'); ?>" alt="help1"/>
                    <h4 class="techno-name"><?php the_sub_field('title_servises_item'); ?></h4>
                </a>
            </li>
        <?php endwhile;
        wp_reset_query();
        ?>
    </ul>
</section>