<div class="row related-post">
    <?php foreach ($posts as $post): ?>
        <?php setup_postdata($post); ?>
        <div class="col-xl-4 col-md-6 col-12 post-item">


            <div class="wrap-post-image">

                <a href="<?php the_permalink(); ?>">
                    <?php if (get_the_post_thumbnail()): ?>
                        <div class="post-image"
                             style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);
                                     width: 100%; height: 260px; background-repeat: no-repeat;
                                     background-size: cover;
                                     ">
                        </div>
                    <?php else : ?>
                        <div class="post-image"
                             style="background-color:#0A246A; background-image: url(<?php echo get_template_directory_uri() . '/images/default-post-img.jpg' ?>); width: 100%; height: 260px;">
                        </div>

                    <?php endif; ?>
                </a>
                <h3 class="post-title"><?php the_title();?></h3>

            </div>

            <div class="post-category">
                <a class="link-category">
                    <?php $getslugid = wp_get_post_terms(get_the_ID(), 'resourcestypes'); ?>
                    <?php echo $getslugid[0]->name; ?> | <?php echo get_the_date(); ?>
                </a>
            </div>

        </div>
    <?php endforeach; ?>
</div>

<!--<div class="col-xl-4 col-md-6 col-12 post-item">-->
<!--    -->
<!--    -->
<!---->
<!--    <ul>-->
<!--        --><?php //foreach( $posts as $post): ?>
<!--        --><?php //setup_postdata($post); ?>
<!--        <li>-->
<!--            <a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!---->
<!--            --><?php //endforeach; ?>
<!--    </ul>-->
<!--</div>-->
