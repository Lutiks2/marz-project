$(document).ready(function ($) {
  window.onscroll = function () {
    stickHeaderBlue();
  };

  var navMenu = document.querySelector('.sticky-main');
  var navMenuLinks = document.querySelectorAll('.sticky-main a');
  var navMenuBtn = document.querySelectorAll('.menu-btn span');
  var sticky = '30';

  function stickHeaderBlue() {
    let menuItem = $('.header__nav__li a');

    if (window.pageYOffset > sticky && ($(document).width() < 1100)) {
      navMenu.style.position = 'fixed';
      navMenu.style.backgroundColor = 'rgba(255, 255, 255 ,0.95)';

      navMenuBtn.forEach((item) => {
        item.classList.add('btn-bg');
      })
    }

    else if (window.pageYOffset > sticky && ($(document).width() > 1100)) {
      navMenu.style.position = 'fixed';
      navMenu.style.backgroundColor = 'rgba(255, 255, 255 ,0.95)';

      navMenuLinks.forEach((item) => {
        item.style.color = '#000';
      });


      navMenuBtn.forEach((item) => {
        item.classList.add('btn-bg');
      })
    }

    else {
      navMenu.removeAttribute('style');
      $.map(menuItem, function (item, i) {
        item.removeAttribute('style')
      });
      navMenuBtn.forEach((item) => {
        item.classList.remove('btn-bg');
      })
    }
  }

  // button menu

  $('.menu-btn').on('click', function () {
    let nav = $('.header-navigation');
    $(this).toggleClass('menu-btn_active');
    if (nav.is(':visible')) {
      nav.slideUp()
    } else {
      nav.slideDown()
    }
  });

  // animate

  let wow = new WOW(
      {
        boxClass: 'wow',      // animated element css class (default is wow)
        // animateClass: 'animated', // animation css class (default is animated)
        // animateClass: 'help-img',
        // offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile: false        // trigger animations on mobile devices (true is default)
      }
  );
  wow.init();

  //// search form

  jQuery(document).on('click', '.marz-search', function () {
    var nav = jQuery('.searching')
    jQuery('.marz-search').toggleClass('active')
    if (nav.is(':visible')) {
      nav.slideUp()
    } else {
      nav.slideDown()
    }
  });


  // video


  $('#play-btn').click(function () {
    $('.image-embed').hide();
    $('.play-btn').hide();
    $('.home-embed-container').html($(this).data('video-embed'))
        .addClass('video-padding');
  });


// tab for mobile on page solution and services


  jQuery('.mobile-tab').click(function () {

    jQuery(this).toggleClass('in').next().slideToggle('slow');

    jQuery('.mobile-tab').not(this).removeClass('in').next().slideUp();


  });
// //scroll to element in tab
  $('.mobile-tab').click(function () {
    let index = $('.mobile-tab').index(this);
    if (index === 0) {
      $('html, body').animate({
        scrollTop: $('.mobile-tab').offset().top - 90
      }, 1000);
    } else if (index === 1) {
      $('html, body').animate({
        scrollTop: $('.mobile-tab').offset().top - 40
      }, 1000);
    } else if (index <= 4) {
      $('html, body').animate({
        scrollTop: $('.mobile-tab').offset().top + 10
      }, 1000);
    } else if (index > 4 && index <= 6) {
      $('html, body').animate({
        scrollTop: $('.mobile-tab').offset().top + 200
      }, 1000);
    } else {
      $('html, body').animate({
        scrollTop: $('.mobile-tab').offset().top + 450
      }, 1000);
    }


  });


// tab for desktop on page solution and services
  jQuery(".tab_item").not(":first").hide();
  jQuery(".tab").click(function () {
    jQuery(".tab").removeClass("active").eq(jQuery(this).index()).addClass("active");
    jQuery(".tab_item").hide().eq($(this).index()).fadeIn()

  }).eq(0).addClass("active");


  /// for deleting error message in forms
  $('input').on('input', function () {

    if ($('input').hasClass('wpcf7-not-valid')) {
      $(this).removeClass('wpcf7-not-valid');
      $(this).parent().find('span').css({display: 'none'})
    }
  })

// direct browser to top right away
  if (window.location.hash)
    scroll(0, 0);
// takes care of some browsers issue
  setTimeout(function () {
    scroll(0, 0);
  }, 1);

// links lead to the article pages services and solutions

  tabService(window.location.hash);

  function tabService(hash) {
    if (hash) {

      if ($(document).width() > 768) {

        // smooth scroll to the anchor id
        $('html,body').animate({
          scrollTop: $('#scrollTab').offset().top - 150
        }, 500, 'swing');

        if (hash) {
          console.log('hash');
          console.log(hash)
          $(hash).parent().parent().find('.tab').removeClass("active");
          $('html,body').find(".tab_item").hide();
          $(hash).parent().addClass("active");
          $(hash).parent().click();
        }
        console.log('mmm')
      }
      else {

        if (hash) {
          $(hash + '-mobile').click();
          console.log(window.location.hash + '-mobile')
        }
      }
    }
  }

  $('.techno-link').click(function (event) {
    var href = $(this).attr('href');
    console.log($(href).length);
    if ($(href).length > 0) {
      event.preventDefault();
      tabService(href);
      return false;
    }

  });

//slickSlider
  $('.one-time').slick({
    dots: true,
    infinite: true,
    arrows: false,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
  })
  // for one size slides
      .on('setPosition', function (event, slick) {
        if ($(document).width() > 1090) {
          slick.$slides.css('height', slick.$slideTrack.height() + 'px');
        }
      });

});

///for scroll map

function mapScrollHandler(e) {
  e.target.style.display = 'none';

  e.target.parentNode.onmouseleave = function () {
    e.target.style.display = 'flex';
  };
  e.target.parentNode.touchend = function () {
    e.target.style.display = 'flex';
  };
}






