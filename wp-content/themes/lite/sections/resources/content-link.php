<?php

$link = get_field('link');

$link_url = $link['url'];
$link_title = $link['title'];
$link_target = $link['target'] ? $link['target'] : '_self';

if ($link):
    ?>
    <div class="col-xl-4 col-md-6 col-12 post-item">

        <div class="wrap-post-image">

            <a href="<?php echo esc_url($link_url); ?>">
                <?php if (get_the_post_thumbnail()): ?>
                    <div class="post-image"
                         style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>); width: 100%; height: 260px; background-repeat: no-repeat;
                                 background-size: cover;">
                    </div>
                <?php else : ?>
                    <div class="post-image"
                         style="background-color:#0A246A; background-image: url(<?php echo get_template_directory_uri() . '/images/default-post-img.jpg' ?>); width: 100%; height: 260px; background-repeat: no-repeat;
                                 background-size: cover;">
                    </div>

                <?php endif; ?>
            </a>
            <h3 class="post-title"><?php the_title();?></h3>
        </div>
        <div class="link">
            <a class="link-resources" href="<?php echo esc_url($link_url); ?>"
               target="_blank">Go to the link</a>


        </div>

    </div>
<?php endif; ?>