<?php
?>

<section class="cloud-technologies cloud-technologies-lite">
    <div class="container">
        <div class="cloud-technologies-sub">
            <div class="cloud-technologies-sub-text">
                <h2><?php echo get_field('heading_cloud_technologies','option'); ?></h2>
                <p><?php echo get_field('description_cloud_technologies','option'); ?></p>
            </div>
            <div class="cloud-technologies-sub-button wow fadeIn">
                <a href="#get_in_touch" class="button button-cloud-technologies">
                    <?php echo get_field('button_text_cloud_technologies','option'); ?>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal-get" id="get_in_touch" aria-hidden="true">
    <a href="#/" class="modal-overlay-get"></a>

    <div class="modal-dialog-get">


        <div class="modal-header-get">
            <a href="#/" class="btn-close" aria-hidden="true" id="modal-one-close">
                <img src="<?php echo get_template_directory_uri() ?>/images/icons/ic-close.svg" alt="">
            </a>
        </div>


        <div class="modal-content-get">
            <div class="modal-content-header">
                <h1 class="modal-content-header-title">
                    <?php the_field('title_in_form', 'option'); ?>
                </h1>

                <h5 class="modal-content-header-subtitle">
                    <?php the_field('subtitle_in_form', 'option'); ?>
                </h5>
            </div>

            <div class="form-get-in-block">
                <?php
                $code = get_field('textarea_for_code_or_snippet', 'option');
                echo do_shortcode($code); ?>
            </div>
        </div>


        <!-- Thank you block -->

        <div class="thank-you-block">

            <div class="container">

                <div class="thank-you-text">

                    <?php echo getImage(get_template_directory_uri() . '/images/submit_1.svg'); ?>

                    <h1 class="thank-you-text-title">
                        <?php the_field('thank_title_for_cloud_technologies', 'option'); ?>
                    </h1>

                    <div class="thank-description">
                        <?php the_field('thank_description_for_cloud_technologies', 'option'); ?>
                    </div>

                </div>

            </div>


        </div>

        <!-- /Thank you block -->

    </div>


</div>
<!-- /Modal -->


