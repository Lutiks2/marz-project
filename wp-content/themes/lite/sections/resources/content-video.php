<div class="col-xl-4 col-md-6 col-12 post-item">

    <div class="wrap-post-image">
        <div class="video-content">
            <a  href="<?php the_permalink(); ?>">
                <?php if (get_the_post_thumbnail()): ?>
                    <div class="post-image"
                         style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>); width: 100%; height: 260px; background-repeat: no-repeat;
                                 background-size: cover;">
                    </div>
                <?php else : ?>
                    <div class="post-image"
                         style="background-color:#0A246A; background-image: url(<?php echo get_template_directory_uri() . '/images/default-post-img.jpg' ?>); width: 100%; height: 260px; background-repeat: no-repeat;
                                 background-size: cover;">
                    </div>

                <?php endif; ?>
            </a>
        </div>
        <h3 class="post-title"><?php the_title(); ?></h3>
        <div class="post-category">
            <a class="link-category">
                <?php echo $getslugid[0]->name; ?> | <?php  echo get_the_date(); ?>
            </a>
        </div>
    </div>

</div>