<?php
/**
 * The template for displaying all pages
 *
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme_name
 */

get_header();
?>

<!--    <div id="primary" class="content-area">-->
<!--        <main id="main" class="site-main">-->
<!--            <div class="container all-page">-->
<!--                <h2>--><?php //echo the_title(); ?><!--</h2>-->
<!--                		--><?php
//                		while ( have_posts() ) :
//                			the_post();
//                            the_content();
////                            get_template_part( 'template-parts/content', get_post_type() );
//                		endwhile; // End of the loop.
//                		?>
<!---->
<!---->
<!--            </div>-->
<!--        </main><!-- #main -->-->
<!--    </div><!-- #primary -->-->



    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="container">

                <?php while (have_posts()) : the_post(); ?>

                    <div class="page-settings">

                        <h1 class="main-title">
                            <?php the_title(); ?>
                        </h1>

                        <div><?php the_content(); ?></div>

                    </div>

                <?php endwhile; ?>

            </div>
        </main>
    </div>

<?php




get_footer();
